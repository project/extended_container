<?php

namespace Drupal\Tests\extended_container\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\extended_container\DependencyInjection\Compiler\RegisterServiceSubscribersPass;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\Compiler\ResolveServiceSubscribersPass;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

/**
 * Run tests on the RegisterServiceSubscribersPass class.
 *
 * @package Drupal\Tests\extended_container\Unit
 * @coversDefaultClass \Drupal\extended_container\DependencyInjection\Compiler\RegisterServiceSubscribersPass
 */
class RegisterServiceSubscribersPassTest extends UnitTestCase {

  /**
   * Test that the service tagged implements the ServiceSubscriberInterface.
   *
   * @covers ::process
   */
  public function testInvalidClass() {
    $this->expectException(InvalidArgumentException::class);
    $this->expectExceptionMessage('Service "foo" must implement interface "Symfony\Component\DependencyInjection\ServiceSubscriberInterface".');
    $container = new ContainerBuilder();

    $container->register('foo', CustomDefinition::class)
      ->addTag('container.drupal_service_subscriber');

    (new RegisterServiceSubscribersPass())->process($container);
    (new ResolveServiceSubscribersPass())->process($container);
  }

}
/**
 * Custom definition class.
 */
class CustomDefinition extends Definition {
}
