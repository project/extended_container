<?php

namespace Drupal\extended_container_instanceof;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * This class registers the car pass.
 *
 * This is part of the demo classes.
 *
 * @package Drupal\extended_container_instanceof
 */
class ExtendedContainerInstanceofServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->addCompilerPass(new CarPass());
  }

}
