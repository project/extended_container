<?php

namespace Drupal\extended_container_instanceof\Car;

use Drupal\extended_container_instanceof\CarInterface;

/**
 * Class that represent the Audi car.
 *
 *  This is part of the emo classes.
 *
 * @package Drupal\extended_container_instanceof\Car
 */
class Audi implements CarInterface {

  /**
   * Brand name.
   */
  public function __toString() {
    return 'Audi';
  }

}
