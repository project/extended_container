<?php

namespace Drupal\extended_container_instanceof;

/**
 * This class acts as a manager or a central hub for all car types.
 *
 * It is part of the demo classes.
 *
 * @package Drupal\extended_container_instanceof
 */
class CarCollection {

  /**
   * A list of car models.
   *
   * @var array
   */
  private $carModels = [];

  /**
   * Add a car model.
   *
   * @param \Drupal\extended_container_instanceof\CarInterface $car
   *   The car model.
   */
  public function addCarModel(CarInterface $car) {
    $this->carModels[] = $car;
  }

  /**
   * Get car models.
   *
   * @return array
   *   List of car models.
   */
  public function getCarModels(): array {
    return $this->carModels;
  }

}
