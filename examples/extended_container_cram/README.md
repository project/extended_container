# Example: controller method resolved arguments

INTRODUCTION
-----------------------
This example demonstrates resolving of controller method 
arguments that type hint services.

REQUIREMENTS
------------

This module requires that you patch core using patches 
in the composer.json file included.

RECOMMENDED MODULES
-------------------

Extended_container module.

INSTALLATION
------------

Run:
composer require drupal/extended_container
composer install

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no 
configuration. When enabled.
