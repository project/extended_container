# Example: Binding in service definitions

INTRODUCTION
-----------------------
When you define a bind in the defaults, that bind must be  
used by at-least one service in the application.

This is the reason am making the controller a service so that the container
can notice that this bind is used by atleast one service.

Otherwise a runtime exception will be thrown when trying
to compile the services.

REQUIREMENTS
------------

This module requires that you patch core using patches 
in the composer.json file included.

RECOMMENDED MODULES
-------------------

Extended_container module.

INSTALLATION
------------

Run:
composer require drupal/extended_container
composer install

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no 
configuration. When enabled.
