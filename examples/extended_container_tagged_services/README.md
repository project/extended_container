# Example: Simpler injection of tagged services

INTRODUCTION
-----------------------
In some Drupal applications is common to get all services
tagged with a specific tag. 
The traditional solution is to create a compiler pass, 
find those services and iterate over them. 

However, this is overkill when you just need to get those
tagged services. 
That's why in Symfony 3.4, there is a shortcut to achieve
the same result
without having to create that compiler pass.

This example demonstrates how the above functionality can
be achieved in Drupal.

REQUIREMENTS
------------

This module requires that you patch core using patches 
in the composer.json file included.

RECOMMENDED MODULES
-------------------

Extended_container module.

INSTALLATION
------------

Run:
composer require drupal/extended_container
composer install

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no 
configuration. When enabled.
