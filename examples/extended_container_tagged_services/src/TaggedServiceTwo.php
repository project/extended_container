<?php

namespace Drupal\extended_container_tagged_services;

/**
 * This class is used to demo the use of tagged services.
 *
 * This is a tagged service.
 *
 * @package Drupal\extended_container_tagged_services
 */
class TaggedServiceTwo {

  /**
   * The human readable class name.
   */
  public function __toString() {
    return 'Tagged service 2';
  }

}
