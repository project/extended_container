# Example: Passing PHP constants to a service definition.

INTRODUCTION
-----------------------
This example demonstrates using PHP constants in a service definition.

REQUIREMENTS
------------

This module requires that you patch core using patches 
in the composer.json file included.

RECOMMENDED MODULES
-------------------

Extended_container module.

INSTALLATION
------------

Run:
composer require drupal/extended_container
composer install

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no 
configuration. When enabled.
