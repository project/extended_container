<?php

namespace Drupal\extended_container_autoconfigure\Television;

use Drupal\extended_container_autoconfigure\TelevisionInterface;

/**
 * The class that represents the Philips type of television.
 *
 * This is part of the demo classes.
 *
 * @package Drupal\extended_container_autoconfigure\Television
 */
class Philips implements TelevisionInterface {

  /**
   * The brand name.
   *
   * @return string
   *   The brand name.
   */
  public function __toString() {
    return 'Philips';
  }

}
