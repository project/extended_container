<?php

namespace Drupal\extended_container_autoconfigure;

/**
 * This class acts as a manager or a central hub for all television types.
 *
 * It is part of the demo classes.
 *
 * @package Drupal\extended_container_autoconfigure
 */
class TelevisionCollection {

  /**
   * A list of television models.
   *
   * @var array
   */
  private $televisionModels = [];

  /**
   * Add a television model.
   *
   * @param \Drupal\extended_container_autoconfigure\TelevisionInterface $television
   *   The television model.
   */
  public function addTelevisionModel(TelevisionInterface $television) {
    $this->televisionModels[] = $television;
  }

  /**
   * Get television models.
   *
   * @return array
   *   The television collection.
   */
  public function getTelevisionModels(): array {
    return $this->televisionModels;
  }

}
