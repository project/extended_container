<?php

namespace Drupal\extended_container_autoconfigure;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * This class registers the demo auto-configuration television models.
 *
 * @package Drupal\extended_container_autoconfigure
 */
class ExtendedContainerAutoconfigureServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->registerForAutoconfiguration(TelevisionInterface::class)->addTag('television.model');

    $container->addCompilerPass(new TelevisionPass());
  }

}
