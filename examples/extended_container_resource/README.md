# Example: Resource exclude keywords

INTRODUCTION
-----------------------
This example demostrates making all classes in your module
folder services automatically.

It also shows how you can exclude those classes that you
do not want to automatically become services.

In this example you notice that all the manual work
that was done in the example module
'extended_container_example_autoconfigure' to register services,
all that was skipped
but yet archiving the same result with less lines in
the service definition.
 
 REQUIREMENTS
 ------------
 
 This module requires that you patch core using patches 
 in the composer.json file included.
 
 RECOMMENDED MODULES
 -------------------
 
 Extended_container module.
 
 INSTALLATION
 ------------
 
 Run:
 composer require drupal/extended_container
 composer install
 
 CONFIGURATION
 -------------
 The module has no menu or modifiable settings. There is no 
 configuration. When enabled.
